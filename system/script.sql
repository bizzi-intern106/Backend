CREATE TABLE "user" (
  "id" varchar PRIMARY KEY,
  "username" varchar,
  "password" varchar,
  "role" varchar
);

CREATE TABLE "email" (
  "id" varchar PRIMARY KEY,
  "to" varchar,
  "from" varchar,
  "subject" varchar,
  "content" text,
  "user_id" varchar
);

CREATE TABLE "attachment" (
  "id" integer,
  "url" varchar,
  "file_name" varchar,
  "email_id" varchar,
  PRIMARY KEY ("id", "email_id")
);

CREATE TABLE "invoice" (
  "id" varchar PRIMARY KEY,
  "form" varchar,
  "serial" varchar,
  "name" varchar,
  "created_at" timestamp,
  "currency" varchar,
  "exchange_rate" integer,
  "full_id" varchar,
  "email_id" varchar,
  "buyer_id" integer,
  "seller_id" integer,
  "payment_id" interger
);

CREATE TABLE "seller" (
  "id" integer PRIMARY KEY,
  "name" varchar,
  "tax_code" varchar,
  "address" varchar,
  "telephone" varchar
);

CREATE TABLE "buyer" (
  "id" integer PRIMARY KEY,
  "name" varchar,
  "tax_code" varchar,
  "address" varchar,
  "payment_method" varchar,
  "contract_id" varchar,
  "start_date" timestamp,
  "end_date" timestamp
);

CREATE TABLE "service" (
  "id" integer PRIMARY KEY,
  "index" interger,
  "name" varchar,
  "money" integer,
  "vat_rate" integer,
  "vate_amount" integer,
  "total_money" integer,
  "invoice_id" varchar
);

CREATE TABLE "payment" (
  "id" interger PRIMARY KEY,
  "total_money_before_tax" interger,
  "tax_rate" interger,
  "total_tax_money" interger,
  "total" interger,
  "total_char" varchar
);

ALTER TABLE "email" ADD FOREIGN KEY ("user_id") REFERENCES "user" ("id");

ALTER TABLE "attachment" ADD FOREIGN KEY ("email_id") REFERENCES "email" ("id");

ALTER TABLE "invoice" ADD FOREIGN KEY ("email_id") REFERENCES "email" ("id");

ALTER TABLE "seller" ADD FOREIGN KEY ("id") REFERENCES "invoice" ("seller_id");

ALTER TABLE "buyer" ADD FOREIGN KEY ("id") REFERENCES "invoice" ("buyer_id");

ALTER TABLE "service" ADD FOREIGN KEY ("invoice_id") REFERENCES "invoice" ("id");

ALTER TABLE "payment" ADD FOREIGN KEY ("id") REFERENCES "invoice" ("payment_id");
