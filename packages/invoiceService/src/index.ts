import "module-alias/register";
import 'dotenv/config';
import fastify from "./bootstrap";

const bootstrap = async () => {
    await fastify.listen({ port: 3000 });
    console.log("fastify is listen in port 3000");
}

bootstrap();