import { authOptions, hasuraOptions } from "@config";
import axios from "axios";

const hasuraApiClient = axios.create({
    baseURL: hasuraOptions.hasuraApiUrl,
    headers: {
        "x-hasura-admin-secret": hasuraOptions.hasuraAdminSecret
    }
})

const authApiClient = axios.create({
    baseURL: authOptions.authApiUrl
})

export { hasuraApiClient, authApiClient };