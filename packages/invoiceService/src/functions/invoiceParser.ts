import "module-alias/register";
import { SQSEvent } from "aws-lambda";
import EventBus from "@lib/redis/eventBus";
import ParserXmlEventHandler from "@handlers/parserXmlHandler";

export const xmlInvoiceParser = async (event: SQSEvent) => {
    try {
        console.log("handling event");
        if (event.Records.length === 0) {
            return {
                statusCode: 400,
                body: JSON.stringify({
                    message: "not contains message payload"
                })
            }
        }

        const eventBus = new EventBus();
        const parserXmlEventHandler = new ParserXmlEventHandler();
        eventBus.register(parserXmlEventHandler.event, parserXmlEventHandler);

        // extract event body
        const payload = JSON.parse(event.Records[0].body);
        console.log("payload", payload);

        const message = JSON.parse(payload.Message) as IEvent;
        console.log("message", message);

        await eventBus.handle(message);
        return {
            statusCode: 200,
            body: JSON.stringify({
                message: "handle success"
            })
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: "something bad"
            })
        }
    }
}