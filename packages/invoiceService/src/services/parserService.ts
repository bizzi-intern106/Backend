import { hasuraApiClient } from "@lib/axios"
import { BadRequestError } from "@lib/error"
import { createCredential } from "@utils/authHelper"
import { parseBuyer, parseGoodsService, parseInvoice, parsePayment, parseSeller } from "@utils/xmlInvoiceParserHelper";
import axios from "axios"
import xml from "xml2js";

const createPayment = async (token: string, payment: any) => {
    const mutation = `#graphql
        mutation MyMutation($objects: [payment_insert_input!] = {}) {
            insert_payment(objects: $objects) {
                returning {
                    id
                }
            }
        }
    `

    const res = await hasuraApiClient.post("/", {
        query: mutation,
        variables: {
            objects: payment
        }
    }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })

    if (res.data.errors) {
        console.log(res.data.errors);
        throw new BadRequestError("something wrong when try to create payment");
    }

    return res.data.data.insert_payment.returning[0];
}

const createBuyer = async (token: string, buyer: any) => {
    const mutation = `#graphql
        mutation MyMutation($objects: [buyer_insert_input!] = {}) {
            insert_buyer(objects: $objects) {
                returning {
                    id
                }
            }
        }

    `

    const res = await hasuraApiClient.post("/", {
        query: mutation,
        variables: {
            objects: buyer
        }
    }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })

    if (res.data.errors) {
        console.log(res.data.errors);
        throw new BadRequestError("something wrong when try to create buyer");
    }

    return res.data.data.insert_buyer.returning[0];
}

const createSeller = async (token: string, seller: any) => {
    const mutation = `#graphql
        mutation MyMutation($objects: [seller_insert_input!] = {}) {
            insert_seller(objects: $objects) {
                returning {
                    id
                }
            }
        }
    `

    const res = await hasuraApiClient.post("/", {
        query: mutation,
        variables: {
            objects: seller
        }
    }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })

    if (res.data.errors) {
        console.log(res.data.errors);
        throw new BadRequestError("something wrong when try to create seller");
    }

    return res.data.data.insert_seller.returning[0];
}

const createInvoice = async (token: string, invoice: any) => {
    const mutation = `#graphql
        mutation MyMutation($objects: [invoice_insert_input!] = {}) {
            insert_invoice(objects: $objects) {
                returning {
                    id
                    email_id
                }
            }
        }
    `

    const res = await hasuraApiClient.post("/", {
        query: mutation,
        variables: {
            objects: invoice
        }
    }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })

    if (res.data.errors) {
        console.log(res.data.errors);
        throw new BadRequestError("something wrong when try to create seller");
    }

    return res.data.data.insert_invoice.returning[0];
}

const createBunchOfGoodsService = async (token: string, invoiceId: string, goods: any[]) => {
    const mutation = `#graphql
        mutation MyMutation($objects: [service_insert_input!] = {}) {
                insert_service(objects: $objects) {
                    returning {
                        invoice_id
                        id
                    }
                }
            }
    `
    const res = await Promise.all(
        goods.map(async (good) => {
            const payload = {
                ...good,
                invoice_id: invoiceId
            }

            const res = await hasuraApiClient.post("/", {
                query: mutation,
                variables: {
                    objects: payload
                }
            }, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            });

            if (res.data.errors) {
                console.log(res.data.errors);
                throw new BadRequestError("something wrong when try to create goods service");
            }

            return res.data.data.insert_service.returning[0];
        })
    )

    return res;
}
export const parseXmlInvoiceDetail = async (url: string) => {
    const response = await axios.get(url);

    // create xml parse
    const parser = new xml.Parser();
    const jsonParser = await parser.parseStringPromise(response.data);

    // extract information
    const invoice = parseInvoice(jsonParser.HDon.DLHDon[0].TTChung[0]);
    const buyer = parseBuyer(jsonParser.HDon.DLHDon[0].NDHDon[0].NMua[0]);
    const seller = parseSeller(jsonParser.HDon.DLHDon[0].NDHDon[0].Nban[0]);
    const goods = parseGoodsService(jsonParser.HDon.DLHDon[0].NDHDon[0].DSHHDVu[0]);
    const payment = parsePayment(jsonParser.HDon.DLHDon[0].NDHDon[0].TToan[0]);

    return {
        invoice,
        buyer,
        seller,
        goods,
        payment
    }
}
export const storeBunchOfXmlData = async (emailId: string, xmlJsonFormat: any) => {
    const {
        invoice,
        buyer,
        seller,
        goods,
        payment } = xmlJsonFormat;

    // get token
    const { token } = await createCredential();
    const { accesstoken } = token;

    // create buyer
    const _buyer = await createBuyer(accesstoken, buyer);
    console.log("buyer: ", _buyer);

    // create seller
    const _seller = await createSeller(accesstoken, seller);
    console.log("seller: ", _seller);

    // create payment
    const _payment = await createPayment(accesstoken, payment);
    console.log("payment: ", _payment);

    // create invoice
    const _invoice = await createInvoice(accesstoken, {
        ...invoice,
        email_id: emailId,
        seller_id: seller.id,
        buyer_id: buyer.id,
        payment_id: payment.id
    });
    console.log("invoice: ", _invoice);

    // create goods service
    const service = await createBunchOfGoodsService(accesstoken, invoice.id, goods);
    console.log("goods: ", service);
}
