import Fastify from "fastify";
import { XML_PARSER_CHANNEL } from "@events/constant";

console.log(XML_PARSER_CHANNEL);
const fastify = Fastify({ logger: true });

export default fastify;