import { env } from "process";

export const hasuraOptions = {
    hasuraApiUrl: env.HASURA_CLOUD_ENDPOINT,
    hasuraAdminSecret: env.HASURA_ADMIN_SECRET
}

export const authOptions = {
    authApiUrl: env.AUTH_ENDPOINT,
}

export const serviceOptions = {
    serviceId: env.SERVICE_ID,
    serviceSecret: env.SERVICE_SECRET
}

export const redisOptions = {
    redisUrl: env.REDIS_URL
}