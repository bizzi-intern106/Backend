export default class ParseXmlEvent implements IEvent {
    name: string = "ParseXmlEvent";

    constructor(
        public id: string,
        public file_name: string,
        public email_id: string,
        public url: string,
    ) {
    }
}