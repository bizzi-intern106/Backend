
import "module-alias/register";
import fastify from "../bootstrap";
import {
    getBunchOfMessages,
    getBunchOfMessagesDetail,
    getMessageAttachments,
    storeBunchofMessages
} from "@services/gmailPullerService";

const handler = async (event: any) => {
    try {
        console.log("event", JSON.stringify(event));
        await fastify.ready();
        console.log("fastify ready...");
        const { token } = await fastify.oAuthClient.getAccessToken();
        const messages = await getBunchOfMessages(token!, { maxResults: 10, labelIds: "INBOX" });

        if (messages.length === 0) {
            //TODO: handle
            return {
                body: JSON.stringify({
                    message: "not have new message"
                }),
                statusCode: 400
            }
        }

        const messagesDetail = await getBunchOfMessagesDetail(token!, messages);
        if (messagesDetail.length === 0) {
            //TODO: handle
            return {
                body: JSON.stringify({
                    message: "not found message detail",
                }),
                statusCode: 400
            }
        }

        // get attachments
        const cleanMessage = await Promise.all(
            messagesDetail.map(message => {
                const { id, attachments } = message;

                const promise = getMessageAttachments(token!, id, attachments)
                    .then(attachments => {
                        return {
                            ...message,
                            attachments
                        }
                    });
                return promise;
            })
        )

        await storeBunchofMessages(cleanMessage);
        return {
            body: JSON.stringify({
                message: "success",
            }),
            statusCode: 200
        }
    } catch (err) {
        console.log(err);
        return {
            body: JSON.stringify({
                message: "fail",
            }),
            statusCode: 500
        }
    }
}

export {
    handler
}