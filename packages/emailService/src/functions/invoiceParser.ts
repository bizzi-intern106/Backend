import { SNS } from "aws-sdk";

const publishSNSMessages = async (topic: string, payload: string) => {
    const publisher = new SNS();
    await publisher.publish({ Message: payload, TopicArn: topic }).promise()
}

export const xmlInvoiceParserPublisher = async (event: any, _: any) => {
    try {
        console.log("publishing sns message");
        // await publishSNSMessages();
        const object = JSON.parse(event.body);
        const { payload, topic } = object.event.data.new;
        await publishSNSMessages(topic, payload);
        console.log("publishing the message...");
        return {
            statusCode: 200,
            body: JSON.stringify({
                message: JSON.stringify(object)
            })
        }
    } catch (err) {
        console.log(err);
        return {
            statusCode: 400,
            body: JSON.stringify({
                message: JSON.stringify(err)
            })
        }
    }
}