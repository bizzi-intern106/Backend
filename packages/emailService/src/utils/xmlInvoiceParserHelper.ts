import { v4 as uuidv4 } from 'uuid';

export const parseBuyer = (rawBuyer: any) => {
    const { Ten, MST, HTTToan, DChi, TNgay, DNgay, SHDong } = rawBuyer;
    return {
        id: uuidv4(),
        name: Ten[0],
        tax_code: MST[0],
        payment_method: HTTToan[0],
        address: DChi[0],
        start_date: new Date(TNgay[0]),
        end_date: new Date(DNgay[0]),
        contract_id: SHDong[0],
    }
}

export const parseSeller = (rawSeller: any) => {
    const { NBTen, NBMST, NBDChi, NBDThoai } = rawSeller;
    return {
        id: uuidv4(),
        name: NBTen[0],
        tax_code: NBMST[0],
        address: NBDChi[0],
        telephone: NBDThoai[0],
    }
}

export const parseInvoice = (rawInvoice: any) => {
    const { MSHDon, KHHDon, SHDon, THDon, TDLap, DVTTe, TGia, IDHDon } = rawInvoice;
    return {
        form: MSHDon[0],
        serial: KHHDon[0],
        id: SHDon[0],
        name: THDon[0],
        created_at: new Date(TDLap[0]),
        currency: DVTTe[0],
        exchange_rate: parseInt(TGia[0]),
        full_id: IDHDon[0]
    }
}

export const parsePayment = (rawPayment: any) => {
    const { TgTCThue, TSGTGTang, TgTThue, TgTTTBSo, TgTTTBChu } = rawPayment;
    return {
        id: uuidv4(),
        total_money_before_tax: parseFloat(TgTCThue[0]),
        tax_rate: parseInt(TSGTGTang[0]),
        total_tax_money: parseFloat(TgTThue[0]),
        total: parseFloat(TgTTTBSo[0]),
        total_char: TgTTTBChu[0]
    }
}

export const parseGoodsService = (rawGoodsService: any) => {
    return rawGoodsService.HHDVu.map((HHDVu: any) => {
        const { STT, THHDVu, TTien, TSuat, TGTGTang, TCong } = HHDVu;
        return {
            id: uuidv4(),
            index: parseInt(STT),
            money: parseFloat(TTien[0]),
            vat_rate: parseInt(TSuat[0]),
            vat_amount: parseFloat(TGTGTang[0]),
            total_money: parseFloat(TCong[0]),
            name: THHDVu[0]
        }
    })
}